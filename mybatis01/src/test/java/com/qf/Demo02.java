package com.qf;

import com.qf.dao.AccountDao;
import com.qf.pojo.Account;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/27 14:30
 */
public class Demo02 {
    SqlSession sqlSession;
    AccountDao accountDao;
    @Before //在@Test注解修饰的方法之前执行
    public void init() throws Exception{
        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = builder.build(in);
        sqlSession = sqlSessionFactory.openSession(true);
        accountDao = sqlSession.getMapper(AccountDao.class);
    }
    @After //在@Test注解修饰的方法之后执行
    public void after(){
        sqlSession.close();
    }

    @Test
    public void testFindAll(){
        List<Account> all = accountDao.findAll();
        for(Account ac : all){
            System.out.println(ac);
        }


    }

}
