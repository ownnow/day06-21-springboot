package com.qf;

import com.qf.dao.UserDao;
import com.qf.pojo.User;
import com.qf.vo.QueryVo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/26 17:55
 */

public class Demo01 {
    SqlSession sqlSession;
    UserDao userDao;
    @Before //在@Test注解修饰的方法之前执行
    public void init() throws Exception{
        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = builder.build(in);
        sqlSession = sqlSessionFactory.openSession(true);
        userDao = sqlSession.getMapper(UserDao.class);
    }
    @After //在@Test注解修饰的方法之后执行
    public void after(){
        sqlSession.close();
    }


    @Test
    //查询
    public void testFindUserById() throws IOException {
        User user = userDao.findUserById(41);
        System.out.println(user);
        sqlSession.close();
    }
    @Test
    //增加
    public void testSaveUser(){
        User user = new User();
        user.setUsername("小白");
        user.setBirthday(new Date());
        user.setSex("女");
        user.setAddress("CHN");
        userDao.saveUser(user);
       // sqlSession.commit();
        System.out.println(user.getId());
    }
    @Test
    //修改
    public void testUpdate(){
        User user = new User();
        user.setUsername("小蓝");
        user.setBirthday(new Date());
        user.setSex("男");
        user.setAddress("EA");
        user.setId(56);
        userDao.updateUser(user);
       // sqlSession.commit();
        System.out.println(user.getId());
    }
    @Test
    //删除
    public void testDelete(){
        userDao.deleteUserById(57);
       // sqlSession.commit();
    }
    //测试模糊查询
    @Test
    public void testFindUserByName(){
        List<User> list = userDao.findUserByName("%王%");
        for(User user : list){
            System.out.println(user);
        }
    }
    //测试模糊查询
    @Test
    public void testFindUserByName2(){
        List<User> list = userDao.findUserByName2("王");
        for(User user : list){
            System.out.println(user);
        }
    }
    @Test
    public void testGetTotal(){
        Integer total = userDao.getTotal();
        System.out.println(total);
    }
    @Test
    public void testFindUserBySexAndAddress(){
        List<User> userList = userDao.findUserBySexAndAddress("男","上海");
        for(User user : userList){
            System.out.println(user);
        }
    }


    @Test
    public void testFindByUser(){
        User user = new User();
        user.setSex("男");
        user.setAddress("上海");
        List<User> userList = userDao.findByUser(user);
        for(User u : userList){
            System.out.println(u);
        }
    }
    @Test
    public void testFindUserByIds(){
        QueryVo queryVo = new QueryVo();
        List<Integer> list = new ArrayList<Integer>();
        list.add(41);
        list.add(42);
        list.add(43);
        queryVo.setIds(list);
        List<User> userList = userDao.findUserByIds(queryVo);
        for(User user : userList){
            System.out.println(user);
        }
    }
    @Test
    public void testDeleteUserByIds(){
        QueryVo queryVo = new QueryVo();
        List<Integer> list = new ArrayList<Integer>();
        list.add(50);
        list.add(56);
        queryVo.setIds(list);
        userDao.deleteUserByIds(queryVo);
    }
    @Test
    public void testAddUser(){
        User user = new User();
        user.setUsername("张卫健");
        user.setAddress("香港");
        userDao.addUser(user);
    }


}
