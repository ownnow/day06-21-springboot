package com.qf;

import java.sql.*;

/**
 * @version 1.0
 * 公众号：Java架构栈
 * @Author: 卓不凡
 */
/*
 *       写一个配置文件数据连接的基本信息
 *       加载配置文件
 *       注册驱动，获得连接
 *       获得statement sql语句执行对象
 *       加载sql
 *       执行sql,得到返回结果
 *       Connection  PrepareStatement   ResultSet
 * */
public class Demo01 {
    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet =null;
        try {
            //原生jdbc的开发流程
            //注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //获得连接
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/mybatis", "root", "1234");
            //定义sql
            String sql = "select * from user where  id =? and username =?";
            //获得statement编译sql
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,"");
            //执行  得到结果
             resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                System.out.println(resultSet.getInt("id"));
                System.out.println(resultSet.getString("username"));
                //封装数据到bean
                //创建实体对象
                //通过set方法赋值
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
