package com.qf.dao;

import com.qf.pojo.User;
import com.qf.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/26 17:36
 */
public interface UserDao {
    /*查询所有用户*/
    List<User> findAll();
    void deleteById();
    User findUserById(Integer id);
    //保存用户信息
    public void saveUser(User user);
    //修改用户
    public void updateUser(User user);
    //删除用户
    public void deleteUserById(Integer id);
    //模糊查询
    public List<User> findUserByName(String name);
    //模糊查询2
    public List<User> findUserByName2(String name);
    //聚合函数查询
    public Integer getTotal();
    //传递多个参数的问题 根据性别和地址一起查询
    public List<User> findUserBySexAndAddress(@Param("sex") String sex, @Param("address") String address);
    //复杂条件查询
    public List<User> findByUser(User user);
    //根据id的集合查询用户列表
    public List<User> findUserByIds(QueryVo queryVo);
    //批量删除
    public void deleteUserByIds(QueryVo queryVo);
    //动态添加
    public void addUser(User user);








}
