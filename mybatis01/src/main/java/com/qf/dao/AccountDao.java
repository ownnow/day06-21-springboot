package com.qf.dao;

import com.qf.pojo.Account;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/27 14:26
 */
public interface AccountDao {
    List<Account> findAll();
}
