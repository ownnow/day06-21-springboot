package com.qf;

import com.qf.dao.UserDao;
import com.qf.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

/**
 * @version 1.0
 * 公众号：Java架构栈
 * @Author: 卓不凡
 */
public class Demo02 {
    public static void main(String[] args) throws Exception {
        //1、读取配置文件
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        //2、核心工厂构建对象
        //构建对象使用构建设计模式
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        //3、获取核心工厂
        //在mybatis里面只能有一个核心工厂 ，创建工厂消耗性能，占用资源的操作
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(resourceAsStream);
        //4、获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //5、通过sqlsession去获取指定dao接口的代理对象
        UserDao mapper = sqlSession.getMapper(UserDao.class);
        List<User> list = sqlSession.selectList("com.qf.dao.UserDao.findAll");
        for (User user : list) {
            System.out.println(user);
        }
        list.stream().forEach(o -> System.out.println(o));
       /* //6、通过代理对象去执行sql操作
        List<User> all = mapper.findAll();
        all.stream().forEach(o-> System.out.println(o));
        for (User user : all) {
            System.out.println(user);
        }*/
        //释放资源
        sqlSession.close();
        resourceAsStream.close();
    }
}
