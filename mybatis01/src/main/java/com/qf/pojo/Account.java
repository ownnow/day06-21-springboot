package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 账户实体类
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/27 9:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account implements Serializable {

    private Integer id;
    private Integer uid;
    private Double money;
    private User user;
}

