package com.lzk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/21 19:53
 */
@Configuration
@EnableSwagger2  //开启swagger文档
public class SwaggerConfig {

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(
                        new ApiInfo(
                                //标题
                                "springboot06-21",
                                //描述
                                "swagger测试",
                                //版本
                                "1.0",
                                //服务地址
                                "http://www.baidu.com",
                                //作者信息
                                new Contact("李泽楷", "http://www.baidu.com", "李泽楷@163.com"),
                                //许可证
                                "java2108_ok",
                                //许可证URL
                                "http://www.baidu.com",
                                //扩展信息
                                new ArrayList<>()
                        )
                );
    }
}
