package com.lzk.controller;


import com.lzk.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/21 19:45
 */
@RestController
@RequestMapping("user")
@Api(value = "用户接口",tags = "用户")
public class UserController {
    @RequestMapping("/{id}")
    @ApiOperation(value = "添加用户", notes = "根据ID添加用户", response = User.class)
    @ApiImplicitParam(name = "id", value="用户ID", required=true, dataType = "int")
    public User getUser(@PathVariable long id){
        return new User(id,"username","password","name",11,"qq@qq.com");
    }
}
