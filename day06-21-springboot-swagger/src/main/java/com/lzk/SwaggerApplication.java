package com.lzk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/21 19:44
 */
@SpringBootApplication

public class SwaggerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SwaggerApplication.class,args);
    }
}
