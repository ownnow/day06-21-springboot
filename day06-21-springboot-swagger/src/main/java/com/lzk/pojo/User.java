package com.lzk.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 11:01
 */
@Data
@TableName("tb_user")
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "用户实体", description = "对应数据库表tb_user")
public class User {
    @ApiModelProperty(value = "员工ID")
    @TableId(value = "ID",type = IdType.AUTO)
    private Long id;
    @TableField("USER_NAME")
    private String userName;
    private String password;
    @ApiModelProperty(value = "员工姓名", dataType = "String")
    private String name;
    private Integer age;
    private String email;
}