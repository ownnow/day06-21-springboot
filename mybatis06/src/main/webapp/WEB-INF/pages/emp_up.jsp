<%--
  Created by IntelliJ IDEA.
  User: 李泽楷
  Date: 2022/6/7
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>员工信息修改</title>
</head>
<body>
<h3>修改员工信息</h3>
<form method="post" name="frmup" action="${pageContext.request.contextPath}/admin/emp/doup">
  <input type="hidden" name="id" value="${requestScope.oneEmp.id}"/><br/>
  员工姓名:<input type="text" name="empName" value="${requestScope.oneEmp.empName}"/><br/>
  员工性别:<input type="radio" name="gender" value="1"/>男
  <c:if test="${requestScope.oneEmp.gender==1}">
    checked
  </c:if>
  <input type="radio" name="gender" value="0"/>女
  <c:if test="${requestScope.oneEmp.gender==0}">
    checked
  </c:if>
  <br/>
  员工出生日期:<input type="date" name="birthday" value="<fmt:formatDate value='${requestScope.oneEmp.birthday}' pattern='yyyy-MM-dd'></fmt:formatDate>"/><br/>
  员工入职日期:<input type="date" name="hireDate" value="<fmt:formatDate value='${requestScope.oneEmp.hireDate}' pattern='yyyy-MM-dd'></fmt:formatDate>"/><br/>
  员工薪资:<input type="number" name="salary" value="${requestScope.oneEmp.salary}"/><br/>
  员工地址:<input type="text" name="address" value="${requestScope.oneEmp.address}"/><br/>
  员工所属部门:
      <select name="deptId">
        <option value="0">请选择</option>
        <c:forEach items="${requestScope.deptList}" var="deptList">
          <option value="${deptList.id}">
            <c:if test="${requestScope.oneEmp.department.id==deptList.id}">
              selected
            </c:if>
              ${deptList.deptName}</option>
        </c:forEach>
    </select> <br/>
  <input name="btnadd" type="submit" value="保存"/>
  <input name="reset" type="reset" value="重置"/><br/>
  <a href="${pageContext.request.contextPath}/admin/emp/list">员工列表</a>
</form>
</body>
</html>
