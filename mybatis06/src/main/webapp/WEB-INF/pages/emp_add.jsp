<%--
  Created by IntelliJ IDEA.
  User: 李泽楷
  Date: 2022/6/7
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>员工信息添加</title>
</head>
<body>
<h3>员工信息添加</h3>
<form name="formadd" method="post" action="${pageContext.request.contextPath}/admin/emp/doadd">
  员工姓名:<input type="text" name="empName"/><br/>
  员工性别:<input type="radio" name="gender" value="1"/>男<input type="radio" name="gender" value="0"/>女<br/>
  员工生日:<input type="date" name="birthday"/><br/>
  员工入职时间:<input type="date" name="hireDate"/><br/>
  员工薪资:<input type="number" name="salary"/><br/>
  员工地址:<input type="text" name="address"/><br/>
  员工所属部门:<select name="deptId">
              <option value="0">请选择</option>
                <c:forEach items="${requestScope.deptList}" var="deptList">
                  <option value="${deptList.id}">${deptList.deptName}</option>
                </c:forEach>
            </select> <br/>
  <input name="btnadd" type="submit" value="报存"/>
  <input name="reset" type="reset" value="重置"/><br/>
  <a href="/admin/emp/list">员工列表</a>
</form>
</body>
</html>
