<%--
  Created by IntelliJ IDEA.
  User: 李泽楷
  Date: 2022/6/6
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加部门</title>
</head>
<body>
<h3>部门信息添加</h3>
<form name="formadd" method="post" action="${pageContext.request.contextPath}/admin/dept/doadd">
    部门名称:<input type="text" name="deptName"/><br/>
    创立日期:<input type="date" name="deptDate"/><br/>
    部门描述:<textarea name="deptDesc" rows="5" cols="10"></textarea><br/>
    <input name="btnadd" type="submit" value="报存"/>
    <input name="reset" type="reset" value="重置"/><br/>
    <a href="/admin/dept/list">部门列表</a>
</form>
</body>
</html>
