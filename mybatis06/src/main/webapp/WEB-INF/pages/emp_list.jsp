<%--
  Created by IntelliJ IDEA.
  User: 李泽楷
  Date: 2022/6/7
  Time: 19:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>员工信息管理</h2>
<form name="frmshow" method="post" action="${pageContext.request.contextPath}/admin/emp/list">
  <input >
  <input name="show" type="submit" value="展示信息"/>
</form>
<table >
  <tr>
    <th>员工id</th>
    <th>员工姓名</th>
    <th>员工性别</th>
    <th>员工生日</th>
    <th>员工入职时间</th>
    <th>员工薪资</th>
    <th>员工地址</th>
    <th>员工所属部门</th>
    <th>操作</th>
  </tr>
  <c:forEach items="${requestScope.employees}" var="employees">
    <tr>
      <td>${employees.id}</td>
      <td>${employees.empName}</td>
      <td>${employees.gender}</td>
      <td>
        <fmt:formatDate value="${employees.birthday}" pattern="yyyy-MM-dd"></fmt:formatDate>
      </td>
      <td>
        <fmt:formatDate value="${employees.hireDate}" pattern="yyyy-MM-dd"></fmt:formatDate>
      </td>
      <td>${employees.salary}</td>
      <td>${employees.address}</td>
      <td >
          ${employees.department.deptName}
      </td>
      <td>
        <a href="${pageContext.request.contextPath}/admin/emp/toup/${employees.id}">修改</a>
        <a href="${pageContext.request.contextPath}/admin/emp/dodel/${employees.id}"  onclick="return confirm('是否删除？');">删除</a>
      </td>
    </tr>
  </c:forEach>
  <tr>
    <td>
      <a href="${pageContext.request.contextPath}/admin/emp/toadd">添加</a>
    </td>
  </tr>
</table>
</body>
</html>
