<%--
  Created by IntelliJ IDEA.
  User: 李泽楷
  Date: 2022/6/6
  Time: 19:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>部门展示</title>
</head>
<body>
<h2>部门展示</h2>
<form name="frmshow" method="post" action="${pageContext.request.contextPath}/admin/dept/list">
<input >
    <input name="show" type="submit" value="展示信息"/>
</form>
<table >
    <tr>
        <th>部门id</th>
        <th>部门名称</th>
        <th>部门创建时间</th>
        <th>部门描述</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${requestScope.list}" var="list">
    <tr>
        <td>${list.id}</td>
        <td>${list.deptName}</td>
        <td>
        <fmt:formatDate value="${list.deptDate}" pattern="yyyy-MM-dd"></fmt:formatDate></td>
        <td>${list.deptDesc}</td>
        <td>
            <a href="${pageContext.request.contextPath}/admin/dept/toup/${list.id}">修改</a>
            <a href="${pageContext.request.contextPath}/admin/dept/dodel/${list.id}"  onclick="return confirm('是否删除？');">删除</a>
<%--           --%>
        </td>
    </tr>
    </c:forEach>
    <tr>
        <td>
            <a href="${pageContext.request.contextPath}/admin/dept/toadd">添加</a>
        </td>
    </tr>
</table>
</body>
</html>
