package com.qf.uitls;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 19:09
 */
public class MybatisUtils {
    static  SqlSessionFactory sqlSessionFactory=null;
    static  InputStream in = null;
    static {
        try {
            in = Resources.getResourceAsStream("sqlMapConfig.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static SqlSessionFactory creatSqlSessionFactory(){
        if (sqlSessionFactory==null){
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        }
        return sqlSessionFactory;
    }
    public static void closeSqlSession(SqlSession sqlSession){
        if (sqlSession!=null){
            sqlSession.close();
        }
    }
}
