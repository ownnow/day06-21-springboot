package com.qf.pojo;

import java.io.Serializable;

public class Admin implements Serializable {
    private Integer adminid;

    private String checkedcode;

    private String usernaem;

    private String password;

    private String realme;

    private Integer status;

    private static final long serialVersionUID = 1L;

    public void setCheckedcode(String checkedcode) {
        this.checkedcode = checkedcode;
    }

    public String getCheckedcode() {
        return checkedcode;
    }

    public Integer getAdminid() {
        return adminid;
    }

    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    public String getUsernaem() {
        return usernaem;
    }

    public void setUsernaem(String usernaem) {
        this.usernaem = usernaem == null ? null : usernaem.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getRealme() {
        return realme;
    }

    public void setRealme(String realme) {
        this.realme = realme == null ? null : realme.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", adminid=").append(adminid);
        sb.append(", usernaem=").append(usernaem);
        sb.append(", password=").append(password);
        sb.append(", realme=").append(realme);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Admin other = (Admin) that;
        return (this.getAdminid() == null ? other.getAdminid() == null : this.getAdminid().equals(other.getAdminid()))
            && (this.getUsernaem() == null ? other.getUsernaem() == null : this.getUsernaem().equals(other.getUsernaem()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getRealme() == null ? other.getRealme() == null : this.getRealme().equals(other.getRealme()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAdminid() == null) ? 0 : getAdminid().hashCode());
        result = prime * result + ((getUsernaem() == null) ? 0 : getUsernaem().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getRealme() == null) ? 0 : getRealme().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }
}