package com.qf.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/13 19:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JsonResult {
    private Object data;
    private String errorMsg;
    private int errorCode=200;
}
