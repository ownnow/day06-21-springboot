package com.qf.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.dao.EmployeeDao;
import com.qf.pojo.Employee;
import com.qf.service.EmpService;
import com.qf.uitls.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/6 11:50
 */

@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    EmployeeDao emp;

    @Override
    public List<Employee> findAll(){
        List<Employee> employees=null;
        try {
            System.out.println("222");
            employees = emp.selectAllEmp();
            System.out.println("进入查找方法");
            System.out.println(employees);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("yic方法123");
        }
        return employees;
    }

    @Override
    public Employee findOneEmp(Integer id) {
        Employee employee = null;
        try {
            employee= emp.selectOneById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    @Override
    @Transactional
    public int addEmp(Employee employee) {

        int result=0;

        Employee employee1 = null;
        try {
            employee1 = emp.selectEmpByName(employee.getEmpName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (employee1 !=null){
                 result=-1;
            }else {
                try {
                    result = emp.addEmp(employee);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        return result;
    }

    @Override
    @Transactional
    public int delEmp(Integer id){
        int result=0;
        Employee employee = null;
        try {
            employee = emp.selectOneById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (employee !=null){
                try {
                    result = emp.deleteEmpById(id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                result=-1;
            }
        return result;
    }

    @Override
    @Transactional
    public int changeEmp(Employee employee){
        int result=0;

        Employee employee1 = null;
        try {
            employee1 = emp.selectEmpByName(employee.getEmpName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (employee1==null){
                try {
                    result = emp.updateEmp(employee);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if (employee1.getId().equals(employee.getId())){
                try {
                    result = emp.updateEmp(employee);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                result=-1;
            }
        return result;
    }

    @Override
    public List<Employee> findByDeptId(Integer id) {
        try {
            List<Employee> employees = emp.selectListById(id);
            return employees;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Employee> getAll() {
        List<Employee> allEmpAndDept = emp.findAllEmpAndDept();
        return allEmpAndDept;
    }

    @Override
    public Employee getOneEmpLazy(Integer id) {
        Employee employee = emp.empByIdLazyDept(id);
        return employee;
    }

    @Override
    public PageInfo<Employee> getEmpByPage(Integer pageindex, Integer pagesize) {
        PageHelper.startPage(pageindex,pagesize);
        List<Employee> empAndDept = emp.findAllEmpAndDept();
        System.out.println(empAndDept);
        PageInfo<Employee> pageInfo = new PageInfo<>(empAndDept);
        return pageInfo;
    }
}
