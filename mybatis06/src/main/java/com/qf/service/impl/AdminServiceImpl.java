package com.qf.service.impl;

import com.qf.dao.AdminMapper;
import com.qf.pojo.Admin;
import com.qf.pojo.AdminExample;
import com.qf.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/14 17:09
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin login(Admin admin) {
        Admin realme=null;
        AdminExample example = new AdminExample();
        AdminExample.Criteria criteria = example.createCriteria();
        criteria.andUsernaemEqualTo(admin.getUsernaem());
        List<Admin> admins = adminMapper.selectByExample(example);
        if (admins !=null && admins.size()>0){
            realme=admins.get(0);
            if (realme.getPassword().equals(admin.getPassword())){
                return realme;
            }
        }
        return null;
    }
}
