package com.qf.service;

import com.github.pagehelper.PageInfo;
import com.qf.pojo.Employee;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/6 11:47
 */
public interface EmpService {
    List<Employee> findAll();
    Employee findOneEmp(Integer id);
    int addEmp(Employee employee);
    int delEmp(Integer id);
    int changeEmp(Employee employee);
    List<Employee> findByDeptId(Integer id);
    List<Employee> getAll();
    Employee getOneEmpLazy(Integer id);
    PageInfo<Employee> getEmpByPage(Integer pageindex,Integer pagesize);
}
