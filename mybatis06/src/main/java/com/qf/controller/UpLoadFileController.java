package com.qf.controller;

import com.qf.json.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/15 9:25
 */
@RestController
@Slf4j
public class UpLoadFileController {

    @PostMapping("/upload")
    public JsonResult upload(@RequestParam("myFile") MultipartFile myFile, String title, HttpSession session){
        System.out.println("标题"+title);
        JsonResult jsonResult = new JsonResult();
        try {
            String filename = myFile.getOriginalFilename();
            if (StringUtils.isEmpty(filename)){
                jsonResult.setErrorCode(500);
                jsonResult.setErrorMsg("文件不能为空");
                return jsonResult;
            }
            System.out.println("Filename------"+filename);
            String lastPointIndex = filename.substring(filename.lastIndexOf("."));
            //新文件名
            filename = UUID.randomUUID().toString().replace("-","").concat(lastPointIndex);
            System.out.println("NewFilename------"+filename);
            String dest = session.getServletContext().getRealPath("/files/");
            System.out.println("文件路径----->"+dest);
            File file = new File(dest, filename);
            if (!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
            }
            myFile.transferTo(file);
            jsonResult.setData("/files/"+filename);
        } catch (IOException e) {
            e.printStackTrace();
            jsonResult.setErrorMsg(e.getMessage());
            jsonResult.setErrorCode(500);
        }

        return jsonResult;
    }
}
