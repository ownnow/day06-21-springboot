package com.qf.controller;

import com.google.code.kaptcha.Producer;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/14 19:27
 */
@Controller
@Slf4j
public class KaptchaController {
    @Autowired
    Producer kaptcherProducer;//Producer是kaptcha的核心接口，通过它去创建图片以及随机的字符串
    @RequestMapping(path = "/kaptcha",method = RequestMethod.GET)
    public void getKaptcha(HttpServletResponse response, HttpSession session){
        //生成验证码
        String text = kaptcherProducer.createText();
        BufferedImage image = kaptcherProducer.createImage(text);

        //将验证码存入session
        session.setAttribute("kaptcha", text);

        //将图片输出到浏览器
        response.setContentType("image/png");
        try{
//            ServletOutputStream outputStream = response.getOutputStream();
//            ImageIO.write(image,"png",outputStream);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(response.getOutputStream());
            encoder.encode(image);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
