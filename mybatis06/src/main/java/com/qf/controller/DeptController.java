package com.qf.controller;

import com.qf.pojo.Department;
import com.qf.service.DeptService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/8 18:53
 */
@Controller
@Slf4j
@RequestMapping(path = "/admin/dept")
public class DeptController {
    @Autowired
    private DeptService deptService;

    @RequestMapping(path = "show")
    public String show(Model model) {
        Department oneDept = deptService.findOneDept(1);
        System.out.println(oneDept);
        model.addAttribute("dept", oneDept);
        return "show";
    }

    @RequestMapping(path = "toadd")
    public String toAdd() {
        return "dept_add";
    }

    @RequestMapping(path = "doadd")
    public String doAdd(Department dept, HttpServletResponse response) {
        int result = deptService.addDept(dept);
        response.setContentType("text/html;charset=utf-8");
        if (result > 0) {
            return "redirect:/admin/dept/list";
        } else if (result == -1) {
            try {
                response.getWriter().print("<script>alert('名称重复，添加失败');history.go(-1);</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            try {
                response.getWriter().print("<script>alert('添加失败');history.go(-1);</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    @RequestMapping(path = "list")
    public String toList(Model model){
        List<Department> list= deptService.findAll();
        model.addAttribute("list",list);
        return  "dept_list";
    }
    @RequestMapping(path = "toup/{id}")
    public String toUp(@PathVariable("id") Integer id,Model model) {
        Department oneDept = deptService.findOneDept(id);
        System.out.println(","+oneDept.getDeptDate());
        model.addAttribute("dept",oneDept);
        return "dept_up";
    }
    @RequestMapping(path = "doup")
    public String doUp(Department department,HttpServletResponse response){
        int result = deptService.changeDept(department);
        response.setContentType("text/html;charset=utf-8");
        if (result > 0) {
            return "redirect:/admin/dept/list";
        } else if (result == -1) {
            try {
                response.getWriter().print("<script>alert('修改失败');history.go(-1);</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            try {
                response.getWriter().print("<script>alert('修改失败');history.go(-1);</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    @RequestMapping(path = "dodel/{id}")
    public String doDel(@PathVariable("id") Integer id,HttpServletResponse response) {
        int result = deptService.delDept(id);
        response.setContentType("text/html;charset=utf-8");
        if (result > 0) {
            return "redirect:/admin/dept/list";
        } else {
            try {
                response.getWriter().print("<script>alert('删除失败');history.go(-1);</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}