package com.qf.controller;

import com.qf.json.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/15 17:28
 */
@RestController
@Slf4j
public class DownloadController {
    @RequestMapping( value = "/download",method = RequestMethod.GET)
    public JsonResult download(@RequestParam("filename") String filename, HttpSession session, HttpServletResponse response) {
        JsonResult jsonResult = new JsonResult();
        try {
            String dest = session.getServletContext().getRealPath("/files/");
            System.out.println("下载的文件路径---->"+dest);
            //要下载的文件
            File destFile = new File(dest, filename);

            //告知浏览器以附件的形式去打开该资源
            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));

            IOUtils.copy(new FileInputStream(destFile), response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            jsonResult.setErrorCode(500);
            jsonResult.setErrorMsg(e.getMessage());
        }
        return jsonResult;
    }
}