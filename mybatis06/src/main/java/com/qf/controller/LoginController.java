package com.qf.controller;

import com.qf.json.JsonResult;
import com.qf.pojo.Admin;
import com.qf.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/14 17:07
 */
@RestController
@Slf4j
public class LoginController {
    @Autowired
    private AdminService adminService;
    
    @PostMapping(value = "/dologin" , produces = "application/json;charset=UTF-8")
    public JsonResult login(Admin admin, HttpSession session){
        JsonResult jsonResult = new JsonResult();
        Admin realadmin = adminService.login(admin);
        String kaptcha = session.getAttribute("kaptcha").toString();
        System.out.println(admin.getUsernaem()+"++-----------++++"+admin.getPassword()+"++++++000000000++++++="+admin.getCheckedcode());

        if (admin.getCheckedcode().equalsIgnoreCase(kaptcha)){
            if (realadmin == null){
                jsonResult.setErrorCode(500);
                jsonResult.setErrorMsg("登录失败!");
                System.out.println(jsonResult.getErrorMsg());
            }else {
                jsonResult.setData(realadmin);
                session.setAttribute("loginUser",realadmin);
            }
        }else {
            jsonResult.setErrorCode(500);
            jsonResult.setErrorMsg("验证码错误");
            System.out.println(jsonResult.getErrorMsg());
        }
        return jsonResult;

    }
}
