package com.qf.controller;

import com.qf.pojo.Department;
import com.qf.pojo.Employee;
import com.qf.service.DeptService;
import com.qf.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/9 10:32
 */
@Controller
@Slf4j
@RequestMapping(path = "/admin/emp")
public class EmpController {
    @Autowired
    private EmpService empService;
    @Autowired
    private DeptService deptService;

    @RequestMapping("list")
    public String list(Model model){
        List<Employee> employees = empService.getAll();
        model.addAttribute("employees",employees);
        return "emp_list";
    }
    @RequestMapping("toadd")
    public String toAdd(Model model){
        List<Department> deptList = deptService.findAll();
        model.addAttribute("deptList",deptList);
        return "emp_add";
    }
    @RequestMapping("doadd")
    public String doAdd(Employee employee, HttpServletResponse response) throws IOException {
        int result = empService.addEmp(employee);
        response.setContentType("text/html;charset=utf-8");
        if (result >0){
            return "redirect:/admin/emp/list";
        }else if (result == -1){
            response.getWriter().print("<script>alert('名称重复，添加失败');history.go(-1);</script>");
            return null;
        }else {
            response.getWriter().print("<script>alert('添加失败');history.go(-1);</script>");
        }
       return null;
    }
    @RequestMapping("toup/{id}")
    public String toUp(@PathVariable("id") Integer id,Model model){
        Employee oneEmp = empService.getOneEmpLazy(id);
        model.addAttribute("oneEmp",oneEmp);
        List<Department> deptList = deptService.findAll();
        model.addAttribute("deptList",deptList);
        return "emp_up";
    }
    @RequestMapping("doup")
    public String doUp(Employee employee,HttpServletResponse response) throws IOException {
        int result = empService.changeEmp(employee);
        if (result > 0){
            return "redirect:/admin/emp/list";
        }else{
            response.getWriter().print("<script>alert('修改失败');history.go(-1);</script>");
            return null;
        }
    }
    @RequestMapping("dodel/{id}")
    public String doDel(@PathVariable("id") Integer id,HttpServletResponse response) throws IOException {
        int result = empService.delEmp(id);
        if (result > 0){
            return "redirect:/admin/emp/list";
        }else {
            response.getWriter().print("<script>alert('删除失败');history.go(-1);</script>");
            return null;
        }
    }
}
