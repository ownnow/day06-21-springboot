package com.qf.controller;

import com.github.pagehelper.PageInfo;
import com.qf.json.JsonResult;
import com.qf.pojo.Department;
import com.qf.pojo.Employee;
import com.qf.service.DeptService;
import com.qf.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/11 9:39
 */
@Controller
@RequestMapping("/emp/json")
@Slf4j
public class EmpJsonController {
    @Autowired
    private DeptService deptService;
    @Autowired
    private EmpService empService;

    @GetMapping("list")
    @ResponseBody
    public List<Employee> jsonList(){
        List<Employee> emps = empService.getAll();
        return emps;
    }
    @GetMapping("del/{empid}")
    @ResponseBody
    public JsonResult jsonDel(@PathVariable(name = "empid") Integer empid){
        JsonResult jsonResult = new JsonResult();
        int result = empService.delEmp(empid);
        if (result > 0){
            jsonResult.setData(result);
            jsonResult.setErrorCode(200);
        }else {
            jsonResult.setErrorCode(500);
            jsonResult.setErrorMsg("删除失败");
        }
        return jsonResult;
    }
    @PostMapping ("toadd")
    @ResponseBody
    public JsonResult toAdd(){
        JsonResult jsonResult = new JsonResult();
        List<Department> all = deptService.findAll();
        jsonResult.setData(all);
//        System.out.println(jsonResult+"==================");
        return jsonResult;
    }
    @GetMapping("doadd")
    @ResponseBody
    public JsonResult doAdd(Employee employee){
        JsonResult jsonResult = new JsonResult();
        int result = empService.addEmp(employee);
        if (result >0){
            jsonResult.setData(result);
            jsonResult.setErrorMsg("问题");
        }else {
            jsonResult.setErrorCode(500);
            jsonResult.setErrorMsg("添加失败");
        }
        return jsonResult;
    }
    @GetMapping("toup/{id}")
    @ResponseBody
    public JsonResult toUp(@PathVariable("id") Integer id){
        System.out.println("查询id----------------->"+id);
        JsonResult jsonResult = new JsonResult();
        Employee oneEmp = empService.getOneEmpLazy(id);
        System.out.println("查询id----------------->"+oneEmp);
        jsonResult.setData(oneEmp);
        System.out.println("查询id----------------->"+jsonResult);

        return jsonResult;

    }
    @PostMapping("doup")
    @ResponseBody
    public JsonResult doUp(Employee employee){
//        System.out.println(employee);
        JsonResult jsonResult = new JsonResult();
        int result = empService.changeEmp(employee);
        if (result > 0){
           jsonResult.setData(result);
        }else{
           jsonResult.setErrorMsg("修改失败");
           jsonResult.setErrorCode(500);
        }
        return jsonResult;
    }
    @GetMapping("pagelist/{pageindex}/{pagesize}")
    @ResponseBody
    public JsonResult getEmps(@PathVariable("pageindex") Integer pageindex,@PathVariable("pagesize") Integer pagesize){
        JsonResult jsonResult = new JsonResult();
        PageInfo<Employee> emps = empService.getEmpByPage(pageindex, pagesize);
        jsonResult.setData(emps);
        return jsonResult;
    }
}
