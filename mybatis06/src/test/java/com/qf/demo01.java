package com.qf;

import com.qf.dao.DepartmentDao;
import com.qf.dao.EmployeeDao;
import com.qf.pojo.Department;
import com.qf.pojo.Employee;
import com.qf.service.DeptService;
import com.qf.service.EmpService;
import com.qf.service.impl.DeptServiceImpl;
import com.qf.uitls.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/31 16:21
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:springMybatis.xml")
public class demo01 {
    @Autowired
    DepartmentDao mapper;
    @Autowired
    EmployeeDao mapper1;
    @Autowired
    private DeptService deptService;
    @Autowired
    EmpService empService;





    /**
     * 部门表对员工表
     * 一对多查询
     */
    @Test
    public void testDeptByIdEmp() throws Exception {
        Department department = mapper.deptByIdEmp(1);
        System.out.println(department.getEmployees());
//        if ( ){
//            System.out.println("123");
//        }else {
//            System.out.println("false");
//        }
        System.out.println(department);
    }

    /**
     * 员工表对部门表
     * 一对一查询
     */
    @Test
    public void testEmpByIdDept() throws Exception {
        Employee employee = mapper1.empByIdDept(1);
        System.out.println(employee);
    }

    /**
     * 分布查询，先查询部门表的信息，再获取部门id,后查询员工信息
     */
    @Test
    public void testDeptByIdLazyEmp() throws Exception {

        Department department = mapper.deptByIdLazyEmp(1);

        System.out.println(department);
    }

    /**
     * 分布查询，先查询员工表的信息，再获取部门id,后查询部门信息
     */
    @Test
    public void testEmpByIdLazyDept() throws Exception {

        Employee employee = mapper1.empByIdDept(1);
        System.out.println(employee);
    }
    @Test
    public void testAddEmp() throws Exception {
        Employee employee = new Employee();
        employee.setEmpName("yy");
        employee.setGender(0);
        employee.setSalary(10000);
        employee.setAddress("US");
        employee.setHireDate(new Date());
        employee.setBirthday(new Date());
        employee.setDeptId(2);
        mapper1.addEmp(employee);
        System.out.println(employee);
        System.out.println(employee.getId());
    }
    @Test
    public void testSelectAllDept() {

        List<Department> all = deptService.findAll();
        for (Department a: all
             ) {
            System.out.println(a);
        }
    }
    @Test
    public void testSelectAllEmp(){
        List<Employee> employees;
        try {
             employees = mapper1.selectAllEmp();
             for (Employee e:employees
            ) {
                 System.out.println(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Test
    public void testDelDeptById() throws Exception {
        int i = mapper.deleteDeptById(8);
        System.out.println(i);
    }
    @Test
    public void testgetAll(){
        List<Department> all = deptService.findAll();
        for (Department a: all
             ) {
            System.out.println(a.getId()+","+a.getDeptName());
        }
    }
}