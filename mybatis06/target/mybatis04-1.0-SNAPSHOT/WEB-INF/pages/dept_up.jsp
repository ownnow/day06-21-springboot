<%--
  Created by IntelliJ IDEA.
  User: 李泽楷
  Date: 2022/6/7
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h3>修改部门信息</h3>
<form method="post" name="frmup" action="${pageContext.request.contextPath}/admin/dept/doup">
  <input type="hidden" name="id" value="${requestScope.dept.id}"/><br/>
  部门名称:<input type="text" name="deptName" value="${requestScope.dept.deptName}"/><br/>
  创立日期:<input type="date" name="deptDate" value="<fmt:formatDate value='${requestScope.dept.deptDate}' pattern='yyyy-MM-dd'></fmt:formatDate>"/><br/>
  部门描述:<textarea name="deptDesc" rows="5" cols="10">${requestScope.dept.deptDesc}</textarea><br/>
  <input name="btnadd" type="submit" value="报存"/>
  <input name="reset" type="reset" value="重置"/><br/>
  <a href="/admin/dept/list">部门列表</a>
</form>
</body>
</html>
