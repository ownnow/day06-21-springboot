package com.qf.test;

import com.qf.dao.UserDao;
import com.qf.pojo.User;
import com.qf.uitls.MybatisUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/30 15:42
 */
public class Demo01 {
    SqlSession sqlSession;
    UserDao userDao;
    @Before //在@Test注解修饰的方法之前执行
    public void init() throws Exception{
//        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
//        SqlSessionFactory sqlSessionFactory = builder.build(in);
//        sqlSession = mybatisUtils.openSession(true);
         sqlSession = MybatisUtils.creatSqlSessionFactory().openSession(true);
        userDao = this.sqlSession.getMapper(UserDao.class);
    }
    @After //在@Test注解修饰的方法之后执行
    public void after(){
        sqlSession.close();
    }
    @Test
    public void testFindAll(){
        List<User> users = userDao.findAll();
        for (User user:users) {
            System.out.println(user);
        }
    }
    @Test
    public void testInsertUser(){
        User user = new User();
        user.setUsername("ping");
        user.setBirthday(new Date());
        user.setSex("n");
        user.setAddress("qwe");
        userDao.saveUser(user);
        System.out.println(user);
        System.out.println(user.getId());
    }
}
