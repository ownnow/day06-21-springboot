package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/30 15:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private Integer id;
    private Integer uid;
    private Double money;
    private User user;
}
