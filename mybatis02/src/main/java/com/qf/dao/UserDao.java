package com.qf.dao;

import com.qf.pojo.User;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/30 15:32
 */
public interface UserDao {
    List<User> findAll();
    void saveUser(User user);

}
