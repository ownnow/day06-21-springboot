package com.qf.uitls;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/30 19:09
 */
public class MybatisUtils {
    static  SqlSessionFactory sqlSessionFactory=null;
    static {
        InputStream in = null;
        try {
            in = Resources.getResourceAsStream("SqlMapConfig.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
             sqlSessionFactory = builder.build(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static SqlSessionFactory creatSqlSessionFactory(){

        return sqlSessionFactory;
    }
}
