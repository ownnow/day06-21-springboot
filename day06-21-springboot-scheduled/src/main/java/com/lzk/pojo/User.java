package com.lzk.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 11:01
 */
@Data
@TableName("tb_user")
public class User {
    @TableId(value = "ID",type = IdType.AUTO)
    private Long id;
    @TableField("USER_NAME")
    private String userName;
    private String password;
    private String name;
    private Integer age;
    private String email;
}