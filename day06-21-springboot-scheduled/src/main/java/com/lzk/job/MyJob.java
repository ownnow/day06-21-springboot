package com.lzk.job;

import com.lzk.pojo.User;
import com.lzk.service.UserService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/21 15:42
 */
public class MyJob implements Job {
    @Autowired
    private UserService userService;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("Execute...."+new Date());
        User user = new User();
        user.setName("玩咯");
        user.setAge(15);
        user.setUserName("qqq");
        user.setEmail("qq@qq.com");
        user.setPassword("123");
        boolean save = userService.save(user);
        if (save){
            System.out.println("添加成功");
        }else {
            System.out.println("添加失败");
        }

    }
}
