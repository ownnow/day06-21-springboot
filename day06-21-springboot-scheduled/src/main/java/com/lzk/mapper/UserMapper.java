package com.lzk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzk.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 11:02
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
