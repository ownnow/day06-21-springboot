package com.lzk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzk.pojo.User;


/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 19:52
 */
public interface UserService extends IService<User> {
}
