package com.lzk.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzk.mapper.UserMapper;
import com.lzk.pojo.User;
import com.lzk.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 19:53
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
