package com.qf.controller;

import com.qf.pojo.User;
import com.qf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/21 17:32
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/{id}")
    public User getUser(@PathVariable(value = "id") Integer id){
        User user = new User();
        user.setUserName("pp");
        user.setPassword("111");
        user.setEmail("aa@qq.com");
        user.setAge(22);
        user.setName("tom");
        return user;
    }
}
