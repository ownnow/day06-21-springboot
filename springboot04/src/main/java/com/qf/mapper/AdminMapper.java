package com.qf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 10:48
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
}
