package com.qf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 10:38
 */
@Data
@TableName("t_admin")
public class Admin {
    @TableId(value = "adminid",type = IdType.AUTO)
    private Integer adminid;
    private String usernaem;
    private String password;
    private String realme;
    private Integer status;
}
