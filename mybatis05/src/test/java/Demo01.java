import com.qf.mapper.EmpMapper;
import com.qf.pojo.Emp;
import com.qf.service.EmpService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/11 14:24
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:*.xml")
public class Demo01 {
    @Autowired
    private EmpService empService;

    @Test
    public void test01(){
        List<Emp> allEmp = empService.getAll();
        System.out.println(allEmp);
    }
}
