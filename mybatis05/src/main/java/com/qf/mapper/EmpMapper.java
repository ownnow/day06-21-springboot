package com.qf.mapper;

import com.qf.pojo.Emp;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/11 11:25
 */
public interface EmpMapper {
    /**
     *  查找所有
     * @return 集合
     */
    List<Emp> findAllEmp();

}
