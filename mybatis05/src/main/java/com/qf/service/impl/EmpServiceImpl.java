package com.qf.service.impl;

import com.qf.mapper.EmpMapper;
import com.qf.pojo.Emp;
import com.qf.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/11 14:41
 */
@Service
public class EmpServiceImpl implements EmpService {
    @Autowired
    private EmpMapper empMapper;
    @Override
    public List<Emp> getAll() {
        List<Emp> allEmp = empMapper.findAllEmp();
        return allEmp;
    }
}
