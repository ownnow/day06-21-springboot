package com.qf.service;

import com.qf.pojo.Emp;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/11 14:41
 */
public interface EmpService {
    List<Emp> getAll();
}
