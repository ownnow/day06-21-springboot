package com.lzk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzk.pojo.TbUser;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/22 10:44
 */
public interface TbUserService extends IService<TbUser> {
}
