package com.lzk.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzk.mapper.TbUserMapper;
import com.lzk.pojo.TbUser;
import com.lzk.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/22 10:44
 */
@Service
public class TbUserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements UserDetailsService,TbUserService{
    @Autowired
    private PasswordEncoder passwordEncoder;
    /**
     * 认证方法
     * @param username 前台传入的用户名
     * @return 认证对象
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //权限列表
        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList("");
       //连接数据库
        LambdaQueryWrapper<TbUser> tbUserLambdaQueryWrapper = new LambdaQueryWrapper<>();
         tbUserLambdaQueryWrapper.eq(TbUser::getUsername, username);
        TbUser tbUser = getOne(tbUserLambdaQueryWrapper);
        if (tbUser !=null){
            System.out.println("111----用户名存在----------->");
            User user =  new User(tbUser.getUsername(), tbUser.getPassword(), authorities);
            System.out.println(user.getUsername());
            return user;
        }
        System.out.println("222----用户名不存在----------->");
        //认证失败
        return null;
    }
}
