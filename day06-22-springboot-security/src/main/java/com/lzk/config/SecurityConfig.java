package com.lzk.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/22 10:46
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserDetailsService myUserDetailService;
    /**
     * 认证配置方法
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //1.指定认证过程是哪个认证类完成
        //2.指定加密器
//       String password=passwordEncoder.encode("123");
//        System.out.println(password);
//        auth.inMemoryAuthentication().withUser("tom").password(password).roles("");
        auth.userDetailsService(myUserDetailService).passwordEncoder(passwordEncoder);

    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //自定义配置

        //1.关闭csrf跨站伪造请求
        //security认为只要不是它本身提供的页面资源，就是非法的
        http.csrf().disable();
        //2.自定义登录表单
        http.formLogin()
                //指定登录页
                .loginPage("/login.html")
                //登录的请求URL
                .loginProcessingUrl("/user/login")
                //如果直接访问登录页，未指定目标资源，登录成功后去的目标资源
                .defaultSuccessUrl("/success")
                //指定页面用户名的name属性和密码的name属性，默认就是username和password
                //.usernameParameter().passwordParameter()
                //放行
                .permitAll();

        //4.指定放行的资源。要写在所有认证通过的请求前面
        http.authorizeRequests()
                //指定放行资源的规则
                .antMatchers("/", "/show/name", "/user/register")
                .permitAll();
        //3.拦截规则【一定要指定】
        http.authorizeRequests()
                //任意请求
                .anyRequest()
                //认证通过
                .authenticated();
    }
}
