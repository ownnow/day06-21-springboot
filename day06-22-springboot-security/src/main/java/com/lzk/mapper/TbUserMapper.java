package com.lzk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzk.pojo.TbUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/22 10:43
 */
@Mapper
public interface TbUserMapper extends BaseMapper<TbUser> {
}
