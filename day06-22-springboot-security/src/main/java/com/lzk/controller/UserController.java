package com.lzk.controller;


import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/22 9:35
 */
@RestController
@RequestMapping
public class UserController {
    @RequestMapping("/user")
    public String getUser(){
        System.out.println("进入");
        return "user";
    }
    /**
     * 获取用户名
     * @return
     */
    @GetMapping("/show/name")
    public String showName(){
        //认证对象
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(principal instanceof User) {
            User user = (User) principal;
            System.out.println("---------------");
            System.out.println(user);
            System.out.println(user.getUsername()+"姓名");
            return user.getUsername();
        }
        //未认证过，但是经过Security框架，那么会有一个临时用户 anonymousUser
        return principal.toString();
    }
}
