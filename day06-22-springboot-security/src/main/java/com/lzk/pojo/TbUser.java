package com.lzk.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/22 10:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_user")
public class TbUser {
    private int id;
    private String username;
    private String password;
}
