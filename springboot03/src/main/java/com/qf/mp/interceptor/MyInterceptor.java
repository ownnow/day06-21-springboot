package com.qf.mp.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/14 17:33
 */
public class MyInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object obj = request.getSession().getAttribute("loginUser");
        if (obj !=null){
            return true;
        }else {
          response.sendRedirect("/static/pages/login.html");
            System.out.println("拦截——————————————————————————————————————————————————————————");
            return false;
        }
    }
}
