package com.qf.mp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 20:38
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
            SpringApplication.run(Application.class,args);
    }
}
