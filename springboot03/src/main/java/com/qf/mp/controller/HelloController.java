package com.qf.mp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/20 20:41
 */

@RestController
public class HelloController {
    @RequestMapping(path = "/hello")
    public String hello(){
        return "hello";
    }
}
