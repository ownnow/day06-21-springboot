package com.qf.mp.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 17:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department {
    private Integer id;
    private String deptName;
    private String deptDesc;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date deptDate;
    private List<Employee> employees;
}
