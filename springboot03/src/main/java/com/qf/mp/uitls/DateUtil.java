package com.qf.mp.uitls;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/6 17:47
 */
public class DateUtil {
    /**
     * 字符串转日期
     * @param dateStr 传递日期字符串
     * @param formateStr 传递日期格式（yyyy-mm--dd）
     * @return
     */
    public static Date getDateBySr(String dateStr,String formateStr){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(formateStr);
            Date date;
            date = formatter.parse(dateStr);
            return date;
        } catch (Exception e) {
            return null;
        }
    }
}
