package com.qf.mp.service;

import com.qf.mp.pojo.Department;


import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/2 11:41
 */
public interface DeptService {
    List<Department> findAll();
    int addDept(Department department);
    int changeDept(Department department);
    int delDept(Integer id);
    Department findOneDept(Integer id);
}
