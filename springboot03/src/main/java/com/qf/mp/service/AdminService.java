package com.qf.mp.service;

import com.qf.mp.pojo.Admin;


/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/14 17:00
 */
public interface AdminService {
    Admin login(Admin admin);
}
