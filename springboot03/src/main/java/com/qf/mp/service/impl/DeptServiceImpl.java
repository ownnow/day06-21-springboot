package com.qf.mp.service.impl;


import com.qf.mp.mapper.DepartmentDao;
import com.qf.mp.mapper.EmployeeDao;
import com.qf.mp.pojo.Department;
import com.qf.mp.service.DeptService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/2 11:45
 */
@Service

public class DeptServiceImpl implements DeptService {

    @Autowired
    DepartmentDao dept;
    @Autowired
    EmployeeDao emp;

    @Override
    public List<Department> findAll() {
        List<Department> allDept = null;
        try {
            allDept = dept.findAllDept();
            System.out.println(allDept);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("异常");
        }
        return allDept;
    }

    @Override
    @Transactional
    public int addDept(Department department) {
        int result=0;
        Department dept1 = null;
        try {
            dept1 = dept.selectDeptByName(department.getDeptName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dept1==null){
            try {
                result = dept.addDept(department);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
                result=-1;
            }
        return result;
    }

    @Override
    @Transactional
    public int changeDept(Department department) {
        int result=0;
        Department department1 = null;
        try {
            department1 = dept.selectDeptByName(department.getDeptName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (department1==null){
            try {
                result = dept.updateDept(department);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (department1.getDeptName().equals(department.getDeptName())){
            try {
                result = dept.updateDept(department);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
                 result=-1;
            }
        return result;
    }

    @Override
    @Transactional
    public int delDept(Integer id) {
        int result=0;
        int i=0;
        try {
            i = emp.selectCountEmpByDeptId(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (i == 0){
                try {
                    result = dept.deleteDeptById(id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                result=-1;
            }
        return result;
    }
    @Override
    public Department findOneDept(Integer id) {
        Department department = null;
        try {
            department = dept.selectById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return department;
    }
}
