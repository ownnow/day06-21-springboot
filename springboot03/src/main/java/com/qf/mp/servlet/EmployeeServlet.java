package com.qf.mp.servlet;

import com.qf.mp.pojo.Employee;
import com.qf.mp.service.EmpService;
import com.qf.mp.service.impl.EmpServiceImpl;
import com.qf.mp.uitls.DateUtil;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/7 18:55
 */

@WebServlet(urlPatterns = {"/admin/emp"})
public class EmployeeServlet extends HttpServlet {

    EmpService empService=new EmpServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action=request.getParameter("oo");
        if ("doAdd".equals(action)){
            doAdd(request, response);
        }else if ("doDel".equals(action)){
            doDel(request, response);
        }else if ("toUp".equals(action)){
            toUp(request, response);
        }else if ("doUp".equals(action)) {
            doUp(request, response);
        }else if ("doFind".equals(action)){

        }else if ("doList".equals(action)){
            doList(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    //添加员工
    protected void doAdd(HttpServletRequest request, HttpServletResponse response) throws IOException {
       String empName=request.getParameter("empName");
       int gender=Integer.parseInt(request.getParameter("gender"));
       String date=request.getParameter("birthday");
       Date birthday = DateUtil.getDateBySr("date","yyyy-MM-dd");
       String date1=request.getParameter("hireDate");
       Date hireDate = DateUtil.getDateBySr("date1","yyyy-MM-dd HH:mm:ss");
       int salary=Integer.parseInt(request.getParameter("salary"));
       String address=request.getParameter("address");
       int deptId=Integer.parseInt(request.getParameter("deptId"));
        Employee employee=new Employee();
        employee.setEmpName(empName);
        employee.setGender(gender);
        employee.setBirthday(birthday);
        employee.setHireDate(hireDate);
        employee.setAddress(address);
        employee.setSalary(salary);
        employee.setDeptId(deptId);
        int i=0;
        try {
             i= empService.addEmp(employee);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (i >0){
            response.sendRedirect(request.getContextPath()+"/admin/emp?oo=doList");
        }else if (i==-1){
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().print("<script>alert('姓名重复,添加失败');history.go(-1);</script>");
        }else {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().print("<script>alert('添加失败');history.go(-1);</script>");
        }
    }
    //删除员工
    protected void doDel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id=Integer.parseInt(request.getParameter("id"));
        int result=0;
        try {
             result= empService.delEmp(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(result>0){
            response.sendRedirect(request.getContextPath()+"/admin/emp?oo=doList");
        }else{
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().print("<script>alert('删除失败');history.go(-1);</script>");
        }
    }
    protected void toUp(HttpServletRequest request, HttpServletResponse response){
        int id=Integer.parseInt(request.getParameter("id"));
        try {
            Employee oneEmp = empService.findOneEmp(id);
            request.setAttribute("oneEmp",oneEmp);
            request.getRequestDispatcher("/admin/emp_up.jsp").forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void doUp(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id=Integer.parseInt(request.getParameter("id"));
        String empName=request.getParameter("empName");
        int gender=Integer.parseInt(request.getParameter("gender"));
        String date=request.getParameter("birthday");
        Date birthday = DateUtil.getDateBySr(date,"yyyy-MM-dd");
        String date1=request.getParameter("hireDate");
        Date hireDate = DateUtil.getDateBySr(date1,"yyyy-MM-dd");
        int salary=Integer.parseInt(request.getParameter("salary"));
        String address=request.getParameter("address");
        int deptId=Integer.parseInt(request.getParameter("deptId"));
        Employee employee=new Employee();
        employee.setId(id);
        employee.setEmpName(empName);
        employee.setGender(gender);
        employee.setBirthday(birthday);
        employee.setHireDate(hireDate);
        employee.setAddress(address);
        employee.setSalary(salary);
        employee.setDeptId(deptId);
        int result=0;
        try {
            result = empService.changeEmp(employee);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result >0){
            response.setContentType("text/html;charset=utf-8");
            response.sendRedirect(request.getContextPath()+"/admin/emp?oo=doList");
        }else {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().print("<script>alert('修改失败');history.go(-1);</script>");
        }
    }
    protected void doList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Employee> employees = null;
        try {
            employees = empService.findAll();
        } catch (Exception e) {
            System.out.println("servlet异常");
            e.printStackTrace();
        }
        System.out.println("列表展示的所有员工:"+employees);
            request.setAttribute("employees",employees);
            request.getRequestDispatcher("/admin/emp_list.jsp").forward(request,response);

    }
}
