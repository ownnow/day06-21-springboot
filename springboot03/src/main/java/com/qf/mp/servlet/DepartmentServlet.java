package com.qf.mp.servlet;

import com.qf.mp.pojo.Department;
import com.qf.mp.service.impl.DeptServiceImpl;
import com.qf.mp.uitls.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;


/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/6 11:42
 */
@WebServlet(urlPatterns = {"/admin/dept"})
public class DepartmentServlet extends HttpServlet {

    private DeptServiceImpl deptService=new DeptServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action=req.getParameter("sp");
        if ("doAdd".equals(action)){
            doAdd(req, resp);
        }else if ("doUp".equals(action)){
            try {
                doUp(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if ("doDel".equals(action)){
            try {
                doDel(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if ("doFind".equals(action)){

        }else if ("toUp".equals(action)){
            try {
                toUp(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if ("doList".equals(action)){
            try {
                doList(req, resp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doGet(req,resp);
    }
    //添加部门
    protected void doAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String deptname = req.getParameter("deptName");
        String deptDesc = req.getParameter("deptDesc");
        String creatdate = req.getParameter("deptDate");
        Date deptDate = DateUtil.getDateBySr(creatdate, "yyyy-MM-dd");
        Department department=new Department();
        department.setDeptName(deptname);
        department.setDeptDesc(deptDesc);
        department.setDeptDate(deptDate);

        int result = 0;
        try {
            result = deptService.addDept(department);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result >0){
            resp.sendRedirect(req.getContextPath()+"/admin/dept?op=doList");
        }else if (result==-1){
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().print("<script>alert('名称重复,添加失败');history.go(-1);</script>");
        }else {
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().print("<script>alert('添加失败');history.go(-1);</script>");
        }
    }
    protected void toUp(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        int deptid = Integer.parseInt(req.getParameter("id"));
        System.out.println("前端点击要修改的id:"+deptid);
        Department oneDept = deptService.findOneDept(deptid);
        req.setAttribute("dept",oneDept);
        req.getRequestDispatcher("/admin/dept_up.jsp").forward(req,resp);


    }
    //修改部门信息
    protected void doUp(HttpServletRequest req, HttpServletResponse resp)throws Exception{
        int id=Integer.parseInt(req.getParameter("id"));
        String deptName = req.getParameter("deptName");
        String deptDesc = req.getParameter("deptDesc");
        String creatdate = req.getParameter("deptDate");
        Date deptDate = DateUtil.getDateBySr(creatdate, "yyyy-MM-dd");
        System.out.println("修改的信息:"+id+","+deptName+","+deptDesc+","+deptDate);
        Department department=new Department();
        department.setId(id);
        department.setDeptName(deptName);
        department.setDeptDesc(deptDesc);
        department.setDeptDate(deptDate);
        int result = 0;
        try {
            result = deptService.changeDept(department);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("执行修改方法的返回值:"+result);
        }
        if (result >0){
            resp.setContentType("text/html;charset=utf-8");
            resp.sendRedirect(req.getContextPath()+"/admin/dept?op=doList");
        }else if (result==-1){
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().print("<script>alert('修改失败');history.go(-1);</script>");
        }else {
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().print("<script>alert('修改失败');history.go(-1);</script>");
        }
    }
    //删除部门
    protected void doDel(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        int deptid=Integer.parseInt(req.getParameter("id"));
        System.out.println("前台传值id:"+deptid);
        int result = deptService.delDept(deptid);
        System.out.println("执行删除操作的返回值:"+result);
        if(result>0){
            resp.sendRedirect(req.getContextPath()+"/admin/dept?op=doList");
        }else if(result==-1){
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().print("<script>alert('该部门还有员工不能删除，删除失败');history.go(-1);</script>");
        }else{
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().print("<script>alert('删除失败');history.go(-1);</script>");
        }
    }
    //分页展示
    protected void doList(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        List<Department> list = deptService.findAll();
//        PageInfo<Department> pageInfo = new PageInfo<Department>(list);
//        pageInfo.setPageSize(5);
        req.setAttribute("list",list);
        req.getRequestDispatcher("/admin/dept_list.jsp").forward(req,resp);
    }

}
