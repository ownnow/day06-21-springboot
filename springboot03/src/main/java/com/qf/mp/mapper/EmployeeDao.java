package com.qf.mp.mapper;

import com.qf.pojo.Employee;
import com.qf.vo.QueryVo;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 17:12
 */
public interface EmployeeDao {
    /**
     * 多表查询
     * @param id 部门id
     * @return
     * @exception
     */
   Employee empByIdDept(Integer id);

    /**
     * 根据部门id查询所有员工信息
     * @param id 部门id
     * @return 员工信息集合
     */
   List<Employee> selectListById(Integer id);

    /**
     * 分布式多表查询，根据员工的id查询员工的部门id，再查询部门信息
     * @param id
     * @return
     */
    Employee empByIdLazyDept(Integer id);

    /**
     * 根据员工id查询员工信息
     * @param id 员工id
     * @return 员工信息
     */
    Employee selectOneById(Integer id);

    /**
     * 根据员工id删除员工信息
     * @param id 员工id
     * @return
     */
    int deleteEmpById(Integer id);

    /**
     * 增加员工
     * @param employee
     * @return
     */
    int addEmp(Employee employee);

    /**
     * 更新员工信息
     * @param employee
     * @return
     */
    int updateEmp(Employee employee);

    /**
     * 通过员工姓名查询
     * @param name 姓名
     * @return 员工
     */
    Employee selectEmpByName(String name);

    /**
     * 通过部门id查询员工数量
     * @param id 部门id
     * @return 员工数量
     */
    int selectCountEmpByDeptId(Integer id);

    /**
     * 查找所有员工信息
     * @return 员工信息
     */
    List<Employee> selectAllEmp();

 /**
  * 查找所有员工信息包括所属的部门信息
  * @return
  */
 List<Employee> findAllEmpAndDept();

 /**
  * 批量删除
  * @param queryVo id集合
  */
 public void deleteEmpByIds(QueryVo queryVo);

}
