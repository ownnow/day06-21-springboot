package com.qf.mp.mapper;

import com.qf.mp.pojo.Department;


import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 17:07
 */
public interface DepartmentDao {
   /**
    * @param id 部门表id
    * @return 下属部门的员工信息
    */
   Department deptByIdEmp(Integer id) ;

   /**
    * 分布查询，先查询部门表的信息，再获取部门id,后查询员工信息
    * @param id 部门id
    * @return 下属部门的员工信息
    */
   Department deptByIdLazyEmp(Integer id);

   /**
    * 根据id查询部门信息
    * @param id 部门id
    * @return 部门信息
    */
   Department selectById(Integer id);

   /**
    * 查询所有的部门信息
    * @return 所有的部门信息
    */
   List<Department> findAllDept();

   /**
    * 添加部门
    * @param department
    * @return
    */
   int addDept(Department department);

   /**
    * 删除部门
    * @param id 部门id
    * @return
    */
   int deleteDeptById(Integer id);

   /**
    * 更新部门信息
    * @param department
    * @return
    */
   int updateDept(Department department);
   /**
    * 通过部门名称查询部门
    * @param name 部门名称
    * @return 部门
    */
   Department selectDeptByName(String name);
}
