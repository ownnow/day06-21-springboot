package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 17:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Integer id;
    private String empName;
    private Integer gender;
    private Date birthday;
    private Date hireDate;
    private Integer salary;
    private String address;
    private Integer deptId;
    private Department department;
}
