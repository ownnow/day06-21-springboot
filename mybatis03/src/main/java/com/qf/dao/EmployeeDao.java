package com.qf.dao;

import com.qf.pojo.Employee;

import java.util.List;
import java.util.Map;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 17:12
 */
public interface EmployeeDao {
    Employee findByIdAndDept(Integer id);
    //查询所有员工
    List<Employee> findAllEmployee();
    //根据姓名查找单个员工
    Employee findEmpNameByEmpName(String empName);
    //统计用户数量
    Integer countById();
    //根据性别分组
    List<Map<String,Object>> groupByGender();
    //增加员工
    Integer addEmployee(Employee employee);
}
