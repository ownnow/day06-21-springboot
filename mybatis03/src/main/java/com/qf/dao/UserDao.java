package com.qf.dao;

import com.qf.pojo.User;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 16:23
 */
public interface UserDao {
    List<User> findAll();
}
