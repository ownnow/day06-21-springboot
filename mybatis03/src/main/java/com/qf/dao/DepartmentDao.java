package com.qf.dao;

import com.qf.pojo.Department;
import com.qf.pojo.Employee;

import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/5/30 17:07
 */
public interface DepartmentDao {
   Department findByIdAndEmployees(Integer id);
}
