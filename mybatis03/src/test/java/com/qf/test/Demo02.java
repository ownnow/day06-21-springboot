package com.qf.test;

import com.qf.dao.EmployeeDao;
import com.qf.pojo.Department;
import com.qf.pojo.Employee;
import com.qf.uitls.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/30 19:34
 */
public class Demo02 {
    SqlSession sqlSession;
    EmployeeDao employeeDao;
    @Before
    public void init() throws Exception{
        sqlSession = MybatisUtils.creatSqlSessionFactory().openSession(true);
        employeeDao = sqlSession.getMapper(EmployeeDao.class);
    }

    @After //在@Test注解修饰的方法之后执行
    public void after(){
        sqlSession.close();
    }

    //查询所有员工
    @Test
    public void findAllEmployee(){
        List<Employee> allEmployee = employeeDao.findAllEmployee();
        for (Employee e: allEmployee) {
            System.out.println(e);
        }
    }
    //根据姓名查找单个员工
    @Test
    public void findEmpNameByEmpName(){
        Employee employee = employeeDao.findEmpNameByEmpName("花解语");
        System.out.println(employee);
    }
    //统计用户数量
    @Test
    public void countById(){
        Integer i = employeeDao.countById();
        System.out.println(i);
    }
    //根据性别分组
    @Test
    public void groupByGender(){
        List<Map<String, Object>> maps = employeeDao.groupByGender();
        for (Map<String,Object> m:maps) {
            System.out.println(m);
        }
    }
    //增加员工
    @Test
    public void addEmployee(){
        Employee employee = new Employee();
        employee.setGender(1);
        employee.setBirthday(new Date());
        employee.setEmpName("王平");
        employee.setAddress("四川");
        employee.setDeptId(1);
        employee.setHireDate(new Date());
        employee.setSalary(8000);
        employee.setDepartment(new Department( 1,"wer",null));
        employeeDao.addEmployee(employee);
        System.out.println(employee.getId());
    }
}
