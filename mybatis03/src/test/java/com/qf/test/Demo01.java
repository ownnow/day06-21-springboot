package com.qf.test;

import com.qf.dao.DepartmentDao;
import com.qf.dao.EmployeeDao;
import com.qf.pojo.Department;
import com.qf.pojo.Employee;
import com.qf.uitls.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Test;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @data 2022/5/30 16:27
 */
public class Demo01 {
    SqlSession sqlSession;
    DepartmentDao departmentDao;
    EmployeeDao employeeDao;

//    @Before //在@Test注解修饰的方法之前执行
//    public void init() throws Exception{
//        InputStream in = Resources.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
//        SqlSessionFactory sqlSessionFactory = builder.build(in);
//        sqlSession = sqlSessionFactory.openSession(true);
//        departmentDao = sqlSession.getMapper(DepartmentDao.class);
//    }
    @After //在@Test注解修饰的方法之后执行
    public void after(){
        sqlSession.close();
    }

    @Test
    public void findByIdAndEmployees(){
        sqlSession = MybatisUtils.creatSqlSessionFactory().openSession(true);
         departmentDao = sqlSession.getMapper(DepartmentDao.class);
       Department byIdAndEmployees = departmentDao.findByIdAndEmployees(1);
            System.out.println(byIdAndEmployees);
    }
    @Test
    public void findByIdAndDept(){
        sqlSession = MybatisUtils.creatSqlSessionFactory().openSession(true);
        employeeDao = sqlSession.getMapper(EmployeeDao.class);
        Employee byIdAndDept = employeeDao.findByIdAndDept(1);
        System.out.println(byIdAndDept);
    }
}
