package com.lzk.controller;

import com.lzk.pojo.User;
import com.lzk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @author 李泽楷
 * @version 1.0
 * @date 2022/6/21 17:47
 */
@Controller
@RequestMapping("hello")
public class HelloController {
    @Autowired
    private UserService userService;

    @RequestMapping("show")
    public String show(Model model){
        model.addAttribute("name","王窝");
        model.addAttribute("msg","1234567890");
        model.addAttribute("birth",new Date());
        model.addAttribute("sex","男");
        return "demo";
    }
    @RequestMapping("list")
    public String list(Model model){
        List<User> list = userService.list();
        model.addAttribute("list",list);
        return "demo1";
    }
}
